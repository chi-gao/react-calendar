const MoveType = {
  KNIGNT: 'knight',

  // 新建活动
  NEW_EVENT: 'new-event',

  // 整体移动
  MOVE_EVENT: 'move-event',

  // 向左/向右拉伸/缩短
  STRETCH_EVENT: 'stretch_event',
}

//  月维度行数
const MONTH_TBALE_ROW = 6;

// 月维度列数
const MONTH_TBALE_COL = 7;

const WEEKS = [
  "周日","周一","周二","周三","周四","周五","周六" 
];

const OPACITY_NUMBER = {
  BASE: 0.4,
  ACTIVE_BASE: 0.8,
  TITLE: 0.6,
  ACTIVE_TITLE: 1,
}

export {
  MoveType,
  MONTH_TBALE_ROW,
  MONTH_TBALE_COL,
  WEEKS,
  OPACITY_NUMBER
};