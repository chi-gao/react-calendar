import React, { Component } from "react";
import { DragSource } from "react-dnd";
import { PropTypes } from "prop-types";
import { getEmptyImage } from "react-dnd-html5-backend";

import { MoveType, OPACITY_NUMBER } from "../../constants/constant";
import { cssHexToRGBA, calculateDates, sourceCanDrag } from "../../util/util";
import "./index.scss";

const target = {
  beginDrag(props, monitor) {
    const { event, changeEvent, direction, changeActiveEventId, date } = props;
    changeEvent(
      {
        moveType: MoveType.STRETCH_EVENT,
        direction,
        originDate: date
      },
      {
        ...event
      }
    );
    changeActiveEventId(event.id);
    return {
      event,
      direction
    };
  },
  endDrag(props, monitor) {
    const dropTarget = monitor.getDropResult();
    const { changeEvent, event, direction, changeActiveEventId, onDragEnd } = props;

    changeActiveEventId(0);
    typeof onDragEnd === 'function' && onDragEnd();
  },
  canDrag(props){
    const { canDrag, date } = props;
    return sourceCanDrag(canDrag, MoveType.STRETCH_EVENT, date);
  }
};

// 左右拖拽区域
@DragSource(MoveType.STRETCH_EVENT, target, (connect, monitor) => ({
  connetDragSource: connect.dragSource(),
  dragPreview: connect.dragPreview(),
  isDragging: monitor.isDragging()
}))
export default class MoveArea extends Component {
  componentDidMount() {
    this.props.dragPreview(getEmptyImage(), { captureDraggingState: true });
  }

  render() {
    const { direction, event, connetDragSource, activeEventId, date,  canDrag } = this.props;
    return connetDragSource(
      <span
        className={`rc_move-area ${
          direction === "left" ? "rc_move-left" : "rc_move-right"
        }`}
        style={{
          backgroundColor: cssHexToRGBA(
            event.color,
            activeEventId && activeEventId === event.id
              ? OPACITY_NUMBER.ACTIVE_BASE
              : OPACITY_NUMBER.BASE
          )
        }}
      />
    );
  }
}

MoveArea.propTypes = {
  direction: PropTypes.string.isRequired,
  event: PropTypes.object.isRequired,
  changeEvent: PropTypes.func
};
