import React, { Component } from "react";

export default class EmptyEvent extends Component {
  render() {
    return <li className="rc_event-item rc_empty-event-item" />;
  }
}
