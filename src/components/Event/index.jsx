import React, { Component } from "react";
import PropTypes from "prop-types";
import { DragSource } from "react-dnd";
import { getEmptyImage } from "react-dnd-html5-backend";
import moment from "moment";
import { Tooltip, Form } from "antd";
import _ from 'lodash';
import MoveArea from "../MoveArea/index";
import { MoveType, OPACITY_NUMBER } from "../../constants/constant";
import { cssHexToRGBA, calculateDates, sourceCanDrag } from "../../util/util";

import "./index.scss";

const {Item} = Form;
const target = {
  beginDrag(props) {
    const { event, date, changeEvent, changeActiveEventId } = props;
    changeEvent(
      {
        moveType: MoveType.MOVE_EVENT,
        originDate: date
      },
      {
        ...event
      }
    );
    changeActiveEventId(event.id);
    return {};
  },

  endDrag(props, monitor) {
    const {
      event,
      date,
      changeActiveEventId,
      onDragEnd
    } = props;

    changeActiveEventId(0);
    typeof onDragEnd === "function" && onDragEnd(date, event);
  },

  canDrag(props){
    const {canDrag, date} = props;
    return sourceCanDrag(canDrag, MoveType.MOVE_EVENT, date);
  },
};

@DragSource(MoveType.MOVE_EVENT, target, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  dragPreview: connect.dragPreview()
}))
export default class Event extends Component {
  componentDidMount() {
    this.props.dragPreview(getEmptyImage(), {
      captureDraggingState: true
    });
  }

  renderEventContent() {
    const { eventTooltip, event, date, activeEventId } = this.props;
    const hasLabel = !!event.title;
    const isSunday = date.day() === 0;
    const content = (
      <div
        className={`rc_event-item_content ${
          isSunday && !event.isStart ? "paddingLeft10" : ""
        }`}
        style={{
          backgroundColor: cssHexToRGBA(
            event.color,
            activeEventId && activeEventId === event.id
              ? OPACITY_NUMBER.ACTIVE_BASE
              : OPACITY_NUMBER.BASE
          )
        }}
      >
        {hasLabel &&
          event.isStart && (
            <span
              className="rc_event-item_content_label"
              style={{
                backgroundColor: cssHexToRGBA(
                  event.color,
                  activeEventId && activeEventId === event.id
                    ? OPACITY_NUMBER.ACTIVE_TITLE
                    : OPACITY_NUMBER.TITLE
                )
              }}
            >
              {event.title}
            </span>
          )}
        {event.isStart || isSunday ? event.content : ""}
      </div>
    );
    if(!eventTooltip){
      return content;
    }
    if(_.isBoolean(eventTooltip) && eventTooltip){ 
      const defaultToolTip = <Form className="rc_default-event-tooltip">
       {event.title && <Item label="标题 :   ">
          {event.title}
        </Item>
       }
       <Item label="内容 :  ">
          {event.content}
        </Item>
        <Item label="开始 :  ">
          {moment(event.begin).format("L HH:mm:ss") }
        </Item>
        <Item label="结束 :  ">
          {moment(event.end).format("L HH:mm:ss")}
        </Item>
      </Form>

      return <Tooltip title={defaultToolTip}>
        {content}
      </Tooltip>
    } else if(typeof eventTooltip === 'object'){
      return <Tooltip title={eventTooltip}>
      {content}
    </Tooltip>
    } else if(typeof eventTooltip === 'function'){
      return <Tooltip title={eventTooltip(event)}>
      {content}
    </Tooltip>
    }

    return content;
  }

  render() {
    const {
      event,
      date,
      changeEvent,
      connectDragSource,
      changeActiveEventId,
      activeEventId,
      onEventDbClick,
      onEventContextMenu,
      canDrag,
    } = this.props;
   
    return connectDragSource(
      <li
        className={`rc_event-item ${
          event.isStart ? "rc_event-item-start" : ""
        } ${event.isEnd ? "rc_event-item-end" : ""}`}
        key={`${date.valueOf()}-${event.id}`}
        onDoubleClick={e => {
          e.stopPropagation();
          typeof onEventDbClick === "function" && onEventDbClick();
        }}
        onContextMenu={e => {
          if (typeof onEventContextMenu === "function") {
            e.stopPropagation();
            onEventContextMenu(e, event);
          }
        }}
      >
        {event.isStart && (
          <MoveArea
            direction="left"
            event={event}
            changeEvent={changeEvent}
            changeActiveEventId={changeActiveEventId}
            activeEventId={activeEventId}
            date={date}
            canDrag={canDrag}
          />
        )}
        {this.renderEventContent.bind(this)()}
        {event.isEnd && (
          <MoveArea
            direction="right"
            event={event}
            changeEvent={changeEvent}
            changeActiveEventId={changeActiveEventId}
            activeEventId={activeEventId}
            date={date}
            canDrag={canDrag}
          />
        )}
      </li>
    );
  }
}

Event.propTypes = {
  event: PropTypes.object.isRequired,
  date: PropTypes.object.isRequired,
  changeEvent: PropTypes.func,
  dragPreview: PropTypes.func,
  connectDragSource: PropTypes.func,
  activeEventId: PropTypes.number
};
