import React, { Component } from "react";
import moment from "moment";
import "moment/locale/zh-cn";
import CalendarOperation from "../CalendarOperation/index";
import Calendar from "../Calendar/index"; 

import "./index.scss";

export default class ReactCalendar extends Component {
  constructor(props) {
    super(props);
    const now = moment().startOf("day");
    this.state = {
      currentDate: now,
      nowDate: now
    };
    this.changeCurrentDate = this.changeCurrentDate.bind(this);
  }

  changeCurrentDate(date) {
    this.setState({
      currentDate: date
    });
  }

  render() {
    const {
      className,
      onDbClick,
      onEventDbClick,
      onDragEnd,
      onDragNewEnd,
      eventTooltip,
      onEventContextMenu,
      events,
      canDrag,
      onDateChange,
      canDrop,
    } = this.props;
    const { currentDate, nowDate } = this.state;
    return (
      <div className={`rc_react-calendar ${className || ""}`}>
        <CalendarOperation
          currentDate={currentDate}
          changeCurrentDate={this.changeCurrentDate}
          onDateChange={onDateChange}
        />
        <Calendar
          currentDate={currentDate}
          nowDate={nowDate}
          onDbClick={onDbClick}
          onEventDbClick={onEventDbClick}
          onDragEnd={onDragEnd}
          events={events}
          onDragNewEnd={onDragNewEnd}
          eventTooltip={eventTooltip}
          onEventContextMenu={onEventContextMenu}
          canDrag={canDrag}
          canDrop={canDrop}
        />
      </div>
    );
  }
}
