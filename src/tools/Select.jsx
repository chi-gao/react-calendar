import React, { Component } from "react";
import { Select as ASelect } from "antd";
import PropTypes from "prop-types";
import _ from "lodash";

const { Option } = ASelect;

export default class Select extends Component {
  render() {
    const { dataSource, defaultValue, onChange } = this.props;
    const options = dataSource.map(item => {
      if (_.isObject(item)) {
        return {
          label: item.label,
          value: item.value
        };
      } else {
        return {
          label: item,
          value: item
        };
      }
    });

    return (
      <ASelect defaultValue={defaultValue} onChange={onChange}>
        {options.map(option => {
          return (
            <Option
              title={option.label.toString()}
              key={option.value.toString()}
              value={option.value}
            >
              {option.label}
            </Option>
          );
        })}
      </ASelect>
    );
  }
}

Select.proptypes = {
  dataSource: PropTypes.array
};
