import moment from 'moment';

const events = [
  {
    id: 1,
    begin: moment(),
    end: moment().add(2, 'd'),
    color: '#528AF1',
    title: '',
    content: '张晓',
  }
];

export {
  events,
};