import React from "react";
import ReactDOM from "react-dom";
import moment from 'moment';
import Calendar from "../src/components/ReactCalendar/index";
import { events } from "./mock";

ReactDOM.render(
  <Calendar
    events={events}
    onDateChange={obj => {
      console.log(obj);
    }}
    onDragNewEnd={(date, event) => {
      console.log(date, event, 'newEnd');
    }}
    onEventContextMenu={(e, event) => {
      console.log(e, event);
    }}
    eventTooltip={true}
    canDrag={(date) => {
      const canDrag = date.valueOf() >= moment().startOf('day').valueOf();
      return {
        new: canDrag,
        move: canDrag,
        stretch: canDrag,
      }
    }}
    canDrop={(date) => {
      const canDrag = date.valueOf() >= moment().startOf('day').valueOf();
      return {
        new: canDrag,
        move: canDrag,
        stretch: canDrag,
      }
    }}
  />,
  document.getElementById("app")
);
